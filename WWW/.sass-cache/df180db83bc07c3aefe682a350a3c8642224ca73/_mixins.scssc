3.2.7 (Media Mark)
7f0ee5ccd03c5bc2210302995df62506ad6d7a1e
o:Sass::Tree::RootNode
:@template"D//Roundness
@mixin rounded($radius: $border-radius, $vert : "", $horz : "" ) {
	@if $vert != "" {
		$vert : - $vert;
	} else {
		$vert : $vert;
	}
	@if $horz != "" {
		$horz : - $horz;
	} else {
		$horz : $horz;
	}
  -moz-border-radius#{$vert}#{$horz}: $radius;
  -webkit-border#{$vert}#{$horz}-radius: $radius;
  border#{$vert}#{$horz}-radius: $radius;
}

//Gradient
@mixin gradient($from, $to) {
	background: $to;
	background: -moz-linear-gradient(top, $from 0%, $to 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,$from), color-stop(100%,$to)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, $from 0%,$to 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, $from 0%,$to 100%); /* Opera 11.10+ */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#{$from}', endColorstr='#{$to}'); /* IE8- */
	background: -ms-linear-gradient(top, $from 0%,$to 100%); /* IE10+ */
	background: linear-gradient(to bottom, $from 0%,$to 100%); /* W3C */
}

//Box Shadow
	/* example:
	@include box-shadow(-5px,0px,5px,rgba(0,0,0,0.3)); for transparent shadow.
	@include box-shadow(4px,4px,2px,#000,true); for inset shadow.
	*/
@mixin box-shadow($top: 2px, $left: 2px, $blur: 2px, $color: #333, $inset: false) {
  @if $inset {
    -webkit-box-shadow:inset $top $left $blur $color;
    -moz-box-shadow:inset $top $left $blur $color;
    box-shadow:inset $top $left $blur $color;
  } @else {
    -webkit-box-shadow: $top $left $blur $color;
    -moz-box-shadow: $top $left $blur $color;
    box-shadow: $top $left $blur $color;
  }
}

//Box Sizing
@mixin box-sizing($type: border-box) {
	-webkit-box-sizing: $type;
	-moz-box-sizing: $type;
	box-sizing: $type;   
}

//Columns
@mixin columns($number : 2, $gap : 2%) {
	position: relative;
	-webkit-column-count: $number;  
	-moz-column-count: $number;     
	column-count: $number; 
	-webkit-column-gap: $gap;
	-moz-column-gap: $gap;
	column-gap: $gap;
}

//Opacity
@mixin opacity ($amount :0.8) {
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=#{$amount * 100})"; // For IE 8 & 9 (more valid) but not required as filter works too
  filter: alpha(opacity=#{$amount * 100}); // This works in IE 8 & 9 too but also 5, 6, 7 */
  -moz-opacity: $amount;
  -khtml-opacity: $amount; /* Safari 1.x */
  opacity: $amount;
}

//Alpha Background
@mixin background-rgba($red: 0, $green: 0, $blue: 0, $alpha: 0.5) {
  background-color: lighten(rgb($red, $green, $blue), 80%);
  background-color: rgba($red, $green, $blue, $alpha);
}

//Flexible Width
@mixin flexible-width ($context, $target) {
  width: (($target / $context) * 100%);
}

@mixin convert-ems ($pixels) {
	font-size: 1 / $base-font * $pixels;
}

//Rotate
@mixin rotate($degrees : 180) {
	//does not support IE8 and below
	-webkit-transform: rotate($degrees#{deg});
	-moz-transform: rotate($degrees#{deg}); 
	-o-transform: rotate($degrees#{deg}); 
	-ms-transform:rotate($degrees#{deg});
	transform:rotate($degrees#{deg});
}

//Scale
@mixin scale($amount : 0.8) {
	-webkit-transform: scale($amount);
	-moz-transform: scale($amount);
	-o-transform: scale($amount);
	-ms-transform: scale($amount);
	transform: scale($amount);  	
}

// Animation
@mixin keyframe ($animation_name) {
    @-webkit-keyframes $animation_name {
        @content;
    }
 
    @-moz-keyframes $animation_name {
        @content;
    }
 
    @-o-keyframes $animation_name {
        @content;
    }
 
    @keyframes $animation_name {
        @content;
    }
}

@mixin animation ($animation, $duration, $iterations, $fill-mode) {
    -webkit-animation: $animation $duration $iterations $fill-mode;
    -moz-animation: $animation $duration $iterations $fill-mode;
    -o-animation: $animation $duration $iterations $fill-mode;
    animation: $animation $duration $iterations $fill-mode;
}

@mixin animation-delay ($delay) {
	-webkit-animation-delay: $delay;
	-moz-animation-delay: $delay;
	-o-animation-delay: $delay;
	animation-delay: $delay;
}

@mixin transition($property: all, $duration: 0.2s, $type: ease-in-out) {
	-webkit-transition: $property, $duration, $type;
	-moz-transition: $property, $duration, $type;
	-o-transition: $property, $duration, $type;
	transition: $property, $duration, $type;
}

@mixin flexbox() {
	display: -webkit-box;
	display: -moz-box;
	display: -ms-flexbox;
	display: -webkit-flex;
	display: flex;
}

@mixin flex($values) {
	-webkit-box-flex: $values;
	-moz-box-flex:  $values;
	-webkit-flex:  $values;
	-ms-flex:  $values;
	flex:  $values;
}

//Responsive Mixin
//Usage example within a css rule ".container { @include respond-to(large-desktop) { width:60%; } }"
//Defaults
// ! IMPORTANT: main.js > UBP.mobileNav... The script needs to be in synch with the mobile value, so if you change it here change it there too!

$large-desktop-value: 1200 !default; // @media (min-width:) { ... }
$desktop-value: 1024 !default; // @media (min-width:) { ... }
$landscape-tablet-value: 979 !default; // @media (min-width:) and (max-width:) { ... }
$portrait-tablet-value: 768 !default;
$landscape-phone-value: 480 !default; // @media (max-width:) { ... }
$portrait-phone-value: 320 !default; // @media (max-width:) { ... }

// Pixels to Ems conversion
$large-desktop: 1 / $base-font-value * $large-desktop-value + em;
$desktop: 1 / $base-font-value * $desktop-value + em;
$landscape-tablet:  1 / $base-font-value *  $landscape-tablet-value + em;
$portrait-tablet: 1 / $base-font-value * $portrait-tablet-value + em;
$landscape-phone: 1 / $base-font-value * $landscape-phone-value + em;
$portrait-phone: 1 / $base-font-value * $portrait-phone-value + em;

@mixin respond-to($point) {
	@if $grid-flexible {
		// Large desktop
		@if $point == large-desktop {
			@media (min-width: $desktop) and (max-width: ($large-desktop - 0.1)) { @content; }
		}
		// Regualar desktop
		@else if $point == desktop {
			@media (min-width: $landscape-tablet) and (max-width: ($desktop - 0.1)) { @content; }
		}
		// Landscape tablet
		@else if $point == landscape-tablet {
			@media (min-width: $portrait-tablet) and (max-width: ($landscape-tablet - 0.1)) and (orientation : landscape) { @content; }
		}
		// Portrait tablet
		@else if $point == portrait-tablet {
			@media (min-width: $landscape-phone) and (max-width: ($portrait-tablet  - 0.1)) and (orientation : portrait) { @content; }
		}
		// All tablet orientations
		@else if $point == tablet {
			@media (min-width: $landscape-phone) and (max-width: ($landscape-tablet - 0.1))  { @content; }
		}
		// Landscape phone
		@else if $point == landscape-phone {
			@media (min-width: $portrait-phone) and (max-width: ($landscape-phone - 0.1)) and (orientation : landscape) { @content; }
		}
		// Portrait phone and down
		@else if $point == portrait-phone {
			@media (max-width: $portrait-phone) and (orientation : portrait) { @content; }
		}
		// All phone orientations
		@else if $point == phone {
			@media (max-width: $landscape-phone)  { @content; }
		}
	}
}
:@has_childrenT:@options{ :@children[6o:Sass::Tree::CommentNode
:
@type:silent:@value["/*Roundness */;@;	[ :
@lineio:Sass::Tree::MixinDefNode:
@name"rounded;T;@;	[u:Sass::Tree::IfNode�[o:Sass::Script::Operation
:@operand2o:Sass::Script::String	:
@type:string:@value" :@options{ :
@linei:@operand1o:Sass::Script::Variable	:
@name"	vert:@underscored_name"	vert;@	;i;@	:@operator:neq;i0[o:Sass::Tree::VariableNode:@guarded0;"	vert:
@expro:!Sass::Script::UnaryOperation	;@	;:
minus:@operando;	;"	vert;"	vert;@	;i	;i	;@	:@children[ ;i	o:Sass::Tree::RuleNode:
@rule["	else:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i
:@members[o:Sass::Selector::Sequence;[o:#Sass::Selector::SimpleSequence
:@sourceso:Set:
@hash{ :@subject0;@;i
;[o:Sass::Selector::Element	;["	else;@:@namespace0;i
;T;@:
@tabsi ;	[o:Sass::Tree::VariableNode:@guarded0;"	vert:
@expro:Sass::Script::Variable	;"	vert:@underscored_name"	vert;@;i;@;	[ ;i;i
u;�[o:Sass::Script::Operation
:@operand2o:Sass::Script::String	:
@type:string:@value" :@options{ :
@linei:@operand1o:Sass::Script::Variable	:
@name"	horz:@underscored_name"	horz;@	;i;@	:@operator:neq;i0[o:Sass::Tree::VariableNode:@guarded0;"	horz:
@expro:!Sass::Script::UnaryOperation	;@	;:
minus:@operando;	;"	horz;"	horz;@	;i;i;@	:@children[ ;io;;["	else;o;;" ;i;[o;;[o;
;o;;{ ;0;@,;i;[o;	;["	else;@,;0;i;T;@; i ;	[o;!;"0;"	horz;#o;$	;"	horz;%"	horz;@;i;@;	[ ;i;io:Sass::Tree::PropNode;["-moz-border-radiuso;$	;"	vert;%"	vert;@;io;$	;"	horz;%"	horz;@;i;o;$	;"radius;%"radius;@;i:@prop_syntax:new;@; i ;	[ ;io;&;[	"-webkit-bordero;$	;"	vert;%"	vert;@;io;$	;"	horz;%"	horz;@;i"-radius;o;$	;"radius;%"radius;@;i;';(;@; i ;	[ ;io;&;[	"bordero;$	;"	vert;%"	vert;@;io;$	;"	horz;%"	horz;@;i"-radius;o;$	;"radius;%"radius;@;i;';(;@; i ;	[ ;i:@splat0;i:
@args[[o;$;"radius;%"radius;@o;$	;"border-radius;%"border_radius;@;i[o;$;"	vert;%"	vert;@o:Sass::Script::String	;:string;" ;@;i[o;$;"	horz;%"	horz;@o;+	;;,;" ;@;io;

;;;["/*Gradient */;@;	[ ;io;;"gradient;T;@;	[o;&;["background;o;$	;"to;%"to;@;i;';(;@; i ;	[ ;io;&;["background;o:Sass::Script::Funcall;"-moz-linear-gradient:@keywords{ ;@;)0;i;*[o;+	;:identifier;"top;@;io:Sass::Script::List	:@separator:
space;[o;$	;"	from;%"	from;@;io:Sass::Script::Number:@numerator_units["%:@original"0%;i ;@:@denominator_units[ ;i;@;io;0	;1;2;[o;$	;"to;%"to;@;io;3;4["%;5"	100%;ii;@;6[ ;i;@;i;';(;@; i ;	[ ;io;

;:normal;["/* FF3.6+ */;@;	[ ;io;&;["background;o;-;"-webkit-gradient;.{ ;@;)0;i;*[
o;+	;;/;"linear;@;io;0	;1;2;[o;+	;;/;"	left;@;io;+	;;/;"top;@;i;@;io;0	;1;2;[o;+	;;/;"	left;@;io;+	;;/;"bottom;@;i;@;io;-;"color-stop;.{ ;@;)0;i;*[o;3;4["%;5"0%;i ;@;6[ ;io;$	;"	from;%"	from;@;io;-;"color-stop;.{ ;@;)0;i;*[o;3;4["%;5"	100%;ii;@;6[ ;io;$	;"to;%"to;@;i;';(;@; i ;	[ ;io;

;;7;["/* Chrome,Safari4+ */;@;	[ ;io;&;["background;o;-;"-webkit-linear-gradient;.{ ;@;)0;i;*[o;+	;;/;"top;@;io;0	;1;2;[o;$	;"	from;%"	from;@;io;3;4["%;5"0%;i ;@;6[ ;i;@;io;0	;1;2;[o;$	;"to;%"to;@;io;3;4["%;5"	100%;ii;@;6[ ;i;@;i;';(;@; i ;	[ ;io;

;;7;["/* Chrome10+,Safari5.1+ */;@;	[ ;io;&;["background;o;-;"-o-linear-gradient;.{ ;@;)0;i;*[o;+	;;/;"top;@;io;0	;1;2;[o;$	;"	from;%"	from;@;io;3;4["%;5"0%;i ;@;6[ ;i;@;io;0	;1;2;[o;$	;"to;%"to;@;io;3;4["%;5"	100%;ii;@;6[ ;i;@;i;';(;@; i ;	[ ;io;

;;7;["/* Opera 11.10+ */;@;	[ ;io;&;["filter;o: Sass::Script::Interpolation:@aftero;+	;;/;"');@;i:@originally_textF:	@mido;$	;"to;%"to;@;i:@whitespace_afterF;@:@beforeo;8;9o;+	;;/;"', endColorstr=';@;i;:F;;o;$	;"	from;%"	from;@;i;<F;@;=o;+	;;/;"?progid:DXImageTransform.Microsoft.gradient(startColorstr=';@;i:@whitespace_beforeF;>F;';(;@; i ;	[ ;io;

;;7;["/* IE8- */;@;	[ ;io;&;["background;o;-;"-ms-linear-gradient;.{ ;@;)0;i;*[o;+	;;/;"top;@;io;0	;1;2;[o;$	;"	from;%"	from;@;io;3;4["%;5"0%;i ;@;6[ ;i;@;io;0	;1;2;[o;$	;"to;%"to;@;io;3;4["%;5"	100%;ii;@;6[ ;i;@;i;';(;@; i ;	[ ;io;

;;7;["/* IE10+ */;@;	[ ;io;&;["background;o;-;"linear-gradient;.{ ;@;)0;i ;*[o;0	;1;2;[o;+	;;/;"to;@;i o;+	;;/;"bottom;@;i ;@;i o;0	;1;2;[o;$	;"	from;%"	from;@;i o;3;4["%;5"0%;i ;@;6[ ;i ;@;i o;0	;1;2;[o;$	;"to;%"to;@;i o;3;4["%;5"	100%;ii;@;6[ ;i ;@;i ;';(;@; i ;	[ ;i o;

;;7;["/* W3C */;@;	[ ;i ;)0;i;*[[o;$;"	from;%"	from;@0[o;$;"to;%"to;@0o;

;;;["/*Box Shadow */;@;	[ ;i#o;

;;7;["�/* example:
@include box-shadow(-5px,0px,5px,rgba(0,0,0,0.3)); for transparent shadow.
@include box-shadow(4px,4px,2px,#000,true); for inset shadow.
*/;@;	[ ;i$o;;"box-shadow;T;@;	[u;[o:Sass::Script::Variable	:
@name"
inset:@underscored_name"
inset:@options{ :
@linei)u:Sass::Tree::IfNode�[00[o:Sass::Tree::PropNode:
@name["-webkit-box-shadow:@valueo:Sass::Script::List	:@separator:
space;[	o:Sass::Script::Variable	;"top:@underscored_name"top:@options{ :
@linei.o;	;"	left;"	left;@;i.o;	;"	blur;"	blur;@;i.o;	;"
color;"
color;@;i.;@;i.:@prop_syntax:new;@:
@tabsi :@children[ ;i.o; ;["-moz-box-shadow;o;	;	;
;[	o;	;"top;"top;@;i/o;	;"	left;"	left;@;i/o;	;"	blur;"	blur;@;i/o;	;"
color;"
color;@;i/;@;i/;;;@;i ;[ ;i/o; ;["box-shadow;o;	;	;
;[	o;	;"top;"top;@;i0o;	;"	left;"	left;@;i0o;	;"	blur;"	blur;@;i0o;	;"
color;"
color;@;i0;@;i0;;;@;i ;[ ;i0[o:Sass::Tree::PropNode;["-webkit-box-shadow:@valueo:Sass::Script::List	:@separator:
space;[
o:Sass::Script::String	:
@type:identifier;"
inset;@	;	i*o; 	;"top;"top;@	;	i*o; 	;"	left;"	left;@	;	i*o; 	;"	blur;"	blur;@	;	i*o; 	;"
color;"
color;@	;	i*;@	;	i*:@prop_syntax:new;@	:
@tabsi :@children[ ;	i*o;;["-moz-box-shadow;o;	;;;[
o;	;;;"
inset;@	;	i+o; 	;"top;"top;@	;	i+o; 	;"	left;"	left;@	;	i+o; 	;"	blur;"	blur;@	;	i+o; 	;"
color;"
color;@	;	i+;@	;	i+;;;@	;i ;[ ;	i+o;;["box-shadow;o;	;;;[
o;	;;;"
inset;@	;	i,o; 	;"top;"top;@	;	i,o; 	;"	left;"	left;@	;	i,o; 	;"	blur;"	blur;@	;	i,o; 	;"
color;"
color;@	;	i,;@	;	i,;;;@	;i ;[ ;	i,;)0;i(;*[
[o;$;"top;%"top;@o;3;4["px;5"2px;i;@;6[ ;i([o;$;"	left;%"	left;@o;3;4["px;5"2px;i;@;6[ ;i([o;$;"	blur;%"	blur;@o;3;4["px;5"2px;i;@;6[ ;i([o;$;"
color;%"
color;@o:Sass::Script::Color	:@attrs{	:redi8:
alphai:
greeni8:	bluei8;0;@;i([o;$;"
inset;%"
inset;@o:Sass::Script::Bool;F;@;i(o;

;;;["/*Box Sizing */;@;	[ ;i4o;;"box-sizing;T;@;	[o;&;["-webkit-box-sizing;o;$	;"	type;%"	type;@;i6;';(;@; i ;	[ ;i6o;&;["-moz-box-sizing;o;$	;"	type;%"	type;@;i7;';(;@; i ;	[ ;i7o;&;["box-sizing;o;$	;"	type;%"	type;@;i8;';(;@; i ;	[ ;i8;)0;i5;*[[o;$;"	type;%"	type;@o;+	;;/;"border-box;@;i5o;

;;;["/*Columns */;@;	[ ;i;o;;"columns;T;@;	[o;&;["position;o;+;;/;"relative;@;';(;@; i ;	[ ;i=o;&;["-webkit-column-count;o;$	;"number;%"number;@;i>;';(;@; i ;	[ ;i>o;&;["-moz-column-count;o;$	;"number;%"number;@;i?;';(;@; i ;	[ ;i?o;&;["column-count;o;$	;"number;%"number;@;i@;';(;@; i ;	[ ;i@o;&;["-webkit-column-gap;o;$	;"gap;%"gap;@;iA;';(;@; i ;	[ ;iAo;&;["-moz-column-gap;o;$	;"gap;%"gap;@;iB;';(;@; i ;	[ ;iBo;&;["column-gap;o;$	;"gap;%"gap;@;iC;';(;@; i ;	[ ;iC;)0;i<;*[[o;$;"number;%"number;@o;3;4[ ;5"2;i;@;6[ ;i<[o;$;"gap;%"gap;@o;3;4["%;5"2%;i;@;6[ ;i<o;

;;;["/*Opacity */;@;	[ ;iFo;;"opacity;T;@;	[o;&;["-ms-filter;o:&Sass::Script::StringInterpolation
;9o;+	;;,;");@;iH;;o:Sass::Script::Operation
:@operand2o;3;4[ ;5"100;ii;@;6@;iH:@operand1o;$	;"amount;%"amount;@;iH;@:@operator:
times;iH;@;=o;+	;;,;"5progid:DXImageTransform.Microsoft.Alpha(Opacity=;@;iH;iH;';(;@; i ;	[ ;iHo;

;;;["I/* For IE 8 & 9 (more valid) but not required as filter works too */;@;	[ ;iHo;&;["filter;o;-;"
alpha;.{ ;@;)0;iI;*[o;8;90;:F;;o;G
;Ho;3;4[ ;5"100;ii;@;6@;iI;Io;$	;"amount;%"amount;@;iI;@;J;K;iI;<0;@;=o;8;90;::originally_text;;o;+	;;/;"=;@;iI;<F;@;=o;+	;;/;"opacity;@;iI;iI;>0;iI;>0;';(;@; i ;	[ ;iIo;

;;;["9/* This works in IE 8 & 9 too but also 5, 6, 7 */ */;@;	[ ;iIo;&;["-moz-opacity;o;$	;"amount;%"amount;@;iJ;';(;@; i ;	[ ;iJo;&;["-khtml-opacity;o;$	;"amount;%"amount;@;iK;';(;@; i ;	[ ;iKo;

;;7;["/* Safari 1.x */;@;	[ ;iKo;&;["opacity;o;$	;"amount;%"amount;@;iL;';(;@; i ;	[ ;iL;)0;iG;*[[o;$;"amount;%"amount;@o;3;4[ ;5"0.8;f0.80000000000000004 ��;@;6@;iGo;

;;;["/*Alpha Background */;@;	[ ;iOo;;"background-rgba;T;@;	[o;&;["background-color;o;-;"lighten;.{ ;@;)0;iQ;*[o;-;"rgb;.{ ;@;)0;iQ;*[o;$	;"red;%"red;@;iQo;$	;"
green;%"
green;@;iQo;$	;"	blue;%"	blue;@;iQo;3;4["%;5"80%;iU;@;6[ ;iQ;';(;@; i ;	[ ;iQo;&;["background-color;o;-;"	rgba;.{ ;@;)0;iR;*[	o;$	;"red;%"red;@;iRo;$	;"
green;%"
green;@;iRo;$	;"	blue;%"	blue;@;iRo;$	;"
alpha;%"
alpha;@;iR;';(;@; i ;	[ ;iR;)0;iP;*[	[o;$;"red;%"red;@o;3;4[ ;5"0;i ;@;6@;iP[o;$;"
green;%"
green;@o;3;4[ ;5"0;i ;@;6@;iP[o;$;"	blue;%"	blue;@o;3;4[ ;5"0;i ;@;6@;iP[o;$;"
alpha;%"
alpha;@o;3;4[ ;5"0.5;f0.5;@;6@;iPo;

;;;["/*Flexible Width */;@;	[ ;iUo;;"flexible-width;T;@;	[o;&;["
width;o;G
;Ho;3
;4["%;ii;@;6[ ;iW;Io;G
;Ho;$	;"context;%"context;@;iW;Io;$	;"target;%"target;@;iW;@;J:div;iW;@;J;K;iW;';(;@; i ;	[ ;iW;)0;iV;*[[o;$;"context;%"context;@0[o;$;"target;%"target;@0o;;"convert-ems;T;@;	[o;&;["font-size;o;G
;Ho;$	;"pixels;%"pixels;@;i[;Io;G
;Ho;$	;"base-font;%"base_font;@;i[;Io;3;4[ ;5"1;i;@;6@;i[;@;J;M;i[;@;J;K;i[;';(;@; i ;	[ ;i[;)0;iZ;*[[o;$;"pixels;%"pixels;@0o;

;;;["/*Rotate */;@;	[ ;i^o;;"rotate;T;@;	[o;

;;;["(/*does not support IE8 and below */;@;	[ ;i`o;&;["-webkit-transform;o;-;"rotate;.{ ;@;)0;ia;*[o;8;90;:F;;o;+	;;/;"deg;@;ia;<0;@;=o;$	;"degrees;%"degrees;@;ia;ia;>0;';(;@; i ;	[ ;iao;&;["-moz-transform;o;-;"rotate;.{ ;@;)0;ib;*[o;8;90;:F;;o;+	;;/;"deg;@;ib;<0;@;=o;$	;"degrees;%"degrees;@;ib;ib;>0;';(;@; i ;	[ ;ibo;&;["-o-transform;o;-;"rotate;.{ ;@;)0;ic;*[o;8;90;:F;;o;+	;;/;"deg;@;ic;<0;@;=o;$	;"degrees;%"degrees;@;ic;ic;>0;';(;@; i ;	[ ;ico;&;["-ms-transform;o;-;"rotate;.{ ;@;)0;id;*[o;8;90;:F;;o;+	;;/;"deg;@;id;<0;@;=o;$	;"degrees;%"degrees;@;id;id;>0;';(;@; i ;	[ ;ido;&;["transform;o;-;"rotate;.{ ;@;)0;ie;*[o;8;90;:F;;o;+	;;/;"deg;@;ie;<0;@;=o;$	;"degrees;%"degrees;@;ie;ie;>0;';(;@; i ;	[ ;ie;)0;i_;*[[o;$;"degrees;%"degrees;@o;3;4[ ;5"180;i�;@;6@;i_o;

;;;["/*Scale */;@;	[ ;iho;;"
scale;T;@;	[
o;&;["-webkit-transform;o;-;"
scale;.{ ;@;)0;ij;*[o;$	;"amount;%"amount;@;ij;';(;@; i ;	[ ;ijo;&;["-moz-transform;o;-;"
scale;.{ ;@;)0;ik;*[o;$	;"amount;%"amount;@;ik;';(;@; i ;	[ ;iko;&;["-o-transform;o;-;"
scale;.{ ;@;)0;il;*[o;$	;"amount;%"amount;@;il;';(;@; i ;	[ ;ilo;&;["-ms-transform;o;-;"
scale;.{ ;@;)0;im;*[o;$	;"amount;%"amount;@;im;';(;@; i ;	[ ;imo;&;["transform;o;-;"
scale;.{ ;@;)0;in;*[o;$	;"amount;%"amount;@;in;';(;@; i ;	[ ;in;)0;ii;*[[o;$;"amount;%"amount;@o;3;4[ ;5"0.8;f0.80000000000000004 ��;@;6@;iio;

;;;["/* Animation */;@;	[ ;iqo;;"keyframe;T;@;	[	o:Sass::Tree::DirectiveNode
;T;["@-webkit-keyframes o;$	;"animation_name;%"animation_name;@;is" ;@;	[o:Sass::Tree::ContentNode;@;	[ ;it;iso;N
;T;["@-moz-keyframes o;$	;"animation_name;%"animation_name;@;iw" ;@;	[o;O;@;	[ ;ix;iwo;N
;T;["@-o-keyframes o;$	;"animation_name;%"animation_name;@;i{" ;@;	[o;O;@;	[ ;i|;i{o;N
;T;["@keyframes o;$	;"animation_name;%"animation_name;@;i" ;@;	[o;O;@;	[ ;i{;i;)0;ir;*[[o;$;"animation_name;%"animation_name;@0o;;"animation;T;@;	[	o;&;["-webkit-animation;o;0	;1;2;[	o;$	;"animation;%"animation;@;i�o;$	;"duration;%"duration;@;i�o;$	;"iterations;%"iterations;@;i�o;$	;"fill-mode;%"fill_mode;@;i�;@;i�;';(;@; i ;	[ ;i�o;&;["-moz-animation;o;0	;1;2;[	o;$	;"animation;%"animation;@;i�o;$	;"duration;%"duration;@;i�o;$	;"iterations;%"iterations;@;i�o;$	;"fill-mode;%"fill_mode;@;i�;@;i�;';(;@; i ;	[ ;i�o;&;["-o-animation;o;0	;1;2;[	o;$	;"animation;%"animation;@;i�o;$	;"duration;%"duration;@;i�o;$	;"iterations;%"iterations;@;i�o;$	;"fill-mode;%"fill_mode;@;i�;@;i�;';(;@; i ;	[ ;i�o;&;["animation;o;0	;1;2;[	o;$	;"animation;%"animation;@;i�o;$	;"duration;%"duration;@;i�o;$	;"iterations;%"iterations;@;i�o;$	;"fill-mode;%"fill_mode;@;i�;@;i�;';(;@; i ;	[ ;i�;)0;i;*[	[o;$;"animation;%"animation;@0[o;$;"duration;%"duration;@0[o;$;"iterations;%"iterations;@0[o;$;"fill-mode;%"fill_mode;@0o;;"animation-delay;T;@;	[	o;&;["-webkit-animation-delay;o;$	;"
delay;%"
delay;@;i�;';(;@; i ;	[ ;i�o;&;["-moz-animation-delay;o;$	;"
delay;%"
delay;@;i�;';(;@; i ;	[ ;i�o;&;["-o-animation-delay;o;$	;"
delay;%"
delay;@;i�;';(;@; i ;	[ ;i�o;&;["animation-delay;o;$	;"
delay;%"
delay;@;i�;';(;@; i ;	[ ;i�;)0;i�;*[[o;$;"
delay;%"
delay;@0o;;"transition;T;@;	[	o;&;["-webkit-transition;o;0	;1:
comma;[o;$	;"property;%"property;@;i�o;$	;"duration;%"duration;@;i�o;$	;"	type;%"	type;@;i�;@;i�;';(;@; i ;	[ ;i�o;&;["-moz-transition;o;0	;1;P;[o;$	;"property;%"property;@;i�o;$	;"duration;%"duration;@;i�o;$	;"	type;%"	type;@;i�;@;i�;';(;@; i ;	[ ;i�o;&;["-o-transition;o;0	;1;P;[o;$	;"property;%"property;@;i�o;$	;"duration;%"duration;@;i�o;$	;"	type;%"	type;@;i�;@;i�;';(;@; i ;	[ ;i�o;&;["transition;o;0	;1;P;[o;$	;"property;%"property;@;i�o;$	;"duration;%"duration;@;i�o;$	;"	type;%"	type;@;i�;@;i�;';(;@; i ;	[ ;i�;)0;i�;*[[o;$;"property;%"property;@o;+	;;/;"all;@;i�[o;$;"duration;%"duration;@o;3;4["s;5"	0.2s;f0.20000000000000001 ��;@;6[ ;i�[o;$;"	type;%"	type;@o;+	;;/;"ease-in-out;@;i�o;;"flexbox;T;@;	[
o;&;["display;o;+;;/;"-webkit-box;@;';(;@; i ;	[ ;i�o;&;["display;o;+;;/;"-moz-box;@;';(;@; i ;	[ ;i�o;&;["display;o;+;;/;"-ms-flexbox;@;';(;@; i ;	[ ;i�o;&;["display;o;+;;/;"-webkit-flex;@;';(;@; i ;	[ ;i�o;&;["display;o;+;;/;"	flex;@;';(;@; i ;	[ ;i�;)0;i�;*[ o;;"	flex;T;@;	[
o;&;["-webkit-box-flex;o;$	;"values;%"values;@;i�;';(;@; i ;	[ ;i�o;&;["-moz-box-flex;o;$	;"values;%"values;@;i�;';(;@; i ;	[ ;i�o;&;["-webkit-flex;o;$	;"values;%"values;@;i�;';(;@; i ;	[ ;i�o;&;["-ms-flex;o;$	;"values;%"values;@;i�;';(;@; i ;	[ ;i�o;&;["	flex;o;$	;"values;%"values;@;i�;';(;@; i ;	[ ;i�;)0;i�;*[[o;$;"values;%"values;@0o;

;;;["/*Responsive Mixin
 *Usage example within a css rule ".container { @include respond-to(large-desktop) { width:60%; } }"
 *Defaults
 * ! IMPORTANT: main.js > UBP.mobileNav... The script needs to be in synch with the mobile value, so if you change it here change it there too! */;@;	[ ;i�o;!;""!default;"large-desktop-value;#o;3;4[ ;5"	1200;i�;@;6@;i�;@;	[ ;i�o;

;;;["&/* @media (min-width:) { ... } */;@;	[ ;i�o;!;""!default;"desktop-value;#o;3;4[ ;5"	1024;i ;@;6@;i�;@;	[ ;i�o;

;;;["&/* @media (min-width:) { ... } */;@;	[ ;i�o;!;""!default;"landscape-tablet-value;#o;3;4[ ;5"979;i�;@;6@;i�;@;	[ ;i�o;

;;;["7/* @media (min-width:) and (max-width:) { ... } */;@;	[ ;i�o;!;""!default;"portrait-tablet-value;#o;3;4[ ;5"768;i ;@;6@;i�;@;	[ ;i�o;!;""!default;"landscape-phone-value;#o;3;4[ ;5"480;i�;@;6@;i�;@;	[ ;i�o;

;;;["&/* @media (max-width:) { ... } */;@;	[ ;i�o;!;""!default;"portrait-phone-value;#o;3;4[ ;5"320;i@;@;6@;i�;@;	[ ;i�o;

;;;["&/* @media (max-width:) { ... } */;@;	[ ;i�o;

;;;["#/* Pixels to Ems conversion */;@;	[ ;i�o;!;"0;"large-desktop;#o;G
;Ho;+	;;/;"em;@;i�;Io;G
;Ho;$	;"large-desktop-value;%"large_desktop_value;@;i�;Io;G
;Ho;$	;"base-font-value;%"base_font_value;@;i�;Io;3;4[ ;5"1;i;@;6@;i�;@;J;M;i�;@;J;K;i�;@;J:	plus;i�;@;	[ ;i�o;!;"0;"desktop;#o;G
;Ho;+	;;/;"em;@;i�;Io;G
;Ho;$	;"desktop-value;%"desktop_value;@;i�;Io;G
;Ho;$	;"base-font-value;%"base_font_value;@;i�;Io;3;4[ ;5"1;i;@;6@;i�;@;J;M;i�;@;J;K;i�;@;J;Q;i�;@;	[ ;i�o;!;"0;"landscape-tablet;#o;G
;Ho;+	;;/;"em;@;i�;Io;G
;Ho;$	;"landscape-tablet-value;%"landscape_tablet_value;@;i�;Io;G
;Ho;$	;"base-font-value;%"base_font_value;@;i�;Io;3;4[ ;5"1;i;@;6@;i�;@;J;M;i�;@;J;K;i�;@;J;Q;i�;@;	[ ;i�o;!;"0;"portrait-tablet;#o;G
;Ho;+	;;/;"em;@;i�;Io;G
;Ho;$	;"portrait-tablet-value;%"portrait_tablet_value;@;i�;Io;G
;Ho;$	;"base-font-value;%"base_font_value;@;i�;Io;3;4[ ;5"1;i;@;6@;i�;@;J;M;i�;@;J;K;i�;@;J;Q;i�;@;	[ ;i�o;!;"0;"landscape-phone;#o;G
;Ho;+	;;/;"em;@;i�;Io;G
;Ho;$	;"landscape-phone-value;%"landscape_phone_value;@;i�;Io;G
;Ho;$	;"base-font-value;%"base_font_value;@;i�;Io;3;4[ ;5"1;i;@;6@;i�;@;J;M;i�;@;J;K;i�;@;J;Q;i�;@;	[ ;i�o;!;"0;"portrait-phone;#o;G
;Ho;+	;;/;"em;@;i�;Io;G
;Ho;$	;"portrait-phone-value;%"portrait_phone_value;@;i�;Io;G
;Ho;$	;"base-font-value;%"base_font_value;@;i�;Io;3;4[ ;5"1;i;@;6@;i�;@;J;M;i�;@;J;K;i�;@;J;Q;i�;@;	[ ;i�o;;"respond-to;T;@;	[u;�[o:Sass::Script::Variable	:
@name"grid-flexible:@underscored_name"grid_flexible:@options{ :
@linei�0[o:Sass::Tree::CommentNode
:
@type:silent:@value["/* Large desktop */;@	:@children[ ;	i�u:Sass::Tree::IfNode�[o:Sass::Script::Operation
:@operand2o:Sass::Script::String	:
@type:identifier:@value"large-desktop:@options{ :
@linei�:@operand1o:Sass::Script::Variable	:
@name"
point:@underscored_name"
point;@	;i�;@	:@operator:eq;i�u:Sass::Tree::IfNode�[o:Sass::Script::Operation
:@operand2o:Sass::Script::String	:
@type:identifier:@value"desktop:@options{ :
@linei�:@operand1o:Sass::Script::Variable	:
@name"
point:@underscored_name"
point;@	;i�;@	:@operator:eq;i�u:Sass::Tree::IfNode#[o:Sass::Script::Operation
:@operand2o:Sass::Script::String	:
@type:identifier:@value"landscape-tablet:@options{ :
@linei�:@operand1o:Sass::Script::Variable	:
@name"
point:@underscored_name"
point;@	;i�;@	:@operator:eq;i�u:Sass::Tree::IfNode [o:Sass::Script::Operation
:@operand2o:Sass::Script::String	:
@type:identifier:@value"portrait-tablet:@options{ :
@linei�:@operand1o:Sass::Script::Variable	:
@name"
point:@underscored_name"
point;@	;i�;@	:@operator:eq;i�u:Sass::Tree::IfNode�	[o:Sass::Script::Operation
:@operand2o:Sass::Script::String	:
@type:identifier:@value"tablet:@options{ :
@linei�:@operand1o:Sass::Script::Variable	:
@name"
point:@underscored_name"
point;@	;i�;@	:@operator:eq;i�u:Sass::Tree::IfNode[o:Sass::Script::Operation
:@operand2o:Sass::Script::String	:
@type:identifier:@value"landscape-phone:@options{ :
@linei�:@operand1o:Sass::Script::Variable	:
@name"
point:@underscored_name"
point;@	;i�;@	:@operator:eq;i�u:Sass::Tree::IfNode�[o:Sass::Script::Operation
:@operand2o:Sass::Script::String	:
@type:identifier:@value"portrait-phone:@options{ :
@linei�:@operand1o:Sass::Script::Variable	:
@name"
point:@underscored_name"
point;@	;i�;@	:@operator:eq;i�u:Sass::Tree::IfNode�[o:Sass::Script::Operation
:@operand2o:Sass::Script::String	:
@type:identifier:@value"
phone:@options{ :
@linei�:@operand1o:Sass::Script::Variable	:
@name"
point:@underscored_name"
point;@	;i�;@	:@operator:eq;i�0[o:Sass::Tree::MediaNode:@has_childrenT;
" ;@	:
@tabsi :@children[o:Sass::Tree::ContentNode;@	;[ ;i�:@query[
"(o;	;;	;
"max-width;@	;i�": o;	;"landscape-phone;"landscape_phone;@	;i�");i�[o:Sass::Tree::MediaNode:@has_childrenT;
" ;@	:
@tabsi :@children[o:Sass::Tree::ContentNode;@	;[ ;i�:@query["(o;	;;	;
"max-width;@	;i�": o;	;"portrait-phone;"portrait_phone;@	;i�")"
 and "(o;	;;	;
"orientation;@	;i�": o;	;;	;
"portrait;@	;i�");i�[o:Sass::Tree::MediaNode:@has_childrenT;
" ;@	:
@tabsi :@children[o:Sass::Tree::ContentNode;@	;[ ;i�:@query["(o;	;;	;
"min-width;@	;i�": o;	;"portrait-phone;"portrait_phone;@	;i�")"
 and "(o;	;;	;
"max-width;@	;i�": o; 
;o:Sass::Script::Number:@numerator_units[ :@original"0.1;
f0.10000000000000001 ��;@	:@denominator_units[ ;i�;o;	;"landscape-phone;"landscape_phone;@	;i�;@	;:
minus;i�")@"(o;	;;	;
"orientation;@	;i�": o;	;;	;
"landscape;@	;i�");i�[o:Sass::Tree::MediaNode:@has_childrenT;
" ;@	:
@tabsi :@children[o:Sass::Tree::ContentNode;@	;[ ;i�:@query["(o;	;;	;
"min-width;@	;i�": o;	;"landscape-phone;"landscape_phone;@	;i�")"
 and "(o;	;;	;
"max-width;@	;i�": o; 
;o:Sass::Script::Number:@numerator_units[ :@original"0.1;
f0.10000000000000001 ��;@	:@denominator_units[ ;i�;o;	;"landscape-tablet;"landscape_tablet;@	;i�;@	;:
minus;i�");i�[o:Sass::Tree::MediaNode:@has_childrenT;
" ;@	:
@tabsi :@children[o:Sass::Tree::ContentNode;@	;[ ;i�:@query["(o;	;;	;
"min-width;@	;i�": o;	;"landscape-phone;"landscape_phone;@	;i�")"
 and "(o;	;;	;
"max-width;@	;i�": o; 
;o:Sass::Script::Number:@numerator_units[ :@original"0.1;
f0.10000000000000001 ��;@	:@denominator_units[ ;i�;o;	;"portrait-tablet;"portrait_tablet;@	;i�;@	;:
minus;i�")@"(o;	;;	;
"orientation;@	;i�": o;	;;	;
"portrait;@	;i�");i�[o:Sass::Tree::MediaNode:@has_childrenT;
" ;@	:
@tabsi :@children[o:Sass::Tree::ContentNode;@	;[ ;i�:@query["(o;	;;	;
"min-width;@	;i�": o;	;"portrait-tablet;"portrait_tablet;@	;i�")"
 and "(o;	;;	;
"max-width;@	;i�": o; 
;o:Sass::Script::Number:@numerator_units[ :@original"0.1;
f0.10000000000000001 ��;@	:@denominator_units[ ;i�;o;	;"landscape-tablet;"landscape_tablet;@	;i�;@	;:
minus;i�")@"(o;	;;	;
"orientation;@	;i�": o;	;;	;
"landscape;@	;i�");i�[o:Sass::Tree::MediaNode:@has_childrenT;
" ;@	:
@tabsi :@children[o:Sass::Tree::ContentNode;@	;[ ;i�:@query["(o;	;;	;
"min-width;@	;i�": o;	;"landscape-tablet;"landscape_tablet;@	;i�")"
 and "(o;	;;	;
"max-width;@	;i�": o; 
;o:Sass::Script::Number:@numerator_units[ :@original"0.1;
f0.10000000000000001 ��;@	:@denominator_units[ ;i�;o;	;"desktop;"desktop;@	;i�;@	;:
minus;i�");i�[o:Sass::Tree::MediaNode:@has_childrenT;
" ;@	:
@tabsi :@children[o:Sass::Tree::ContentNode;@	;[ ;i�:@query["(o;	;;	;
"min-width;@	;i�": o;	;"desktop;"desktop;@	;i�")"
 and "(o;	;;	;
"max-width;@	;i�": o; 
;o:Sass::Script::Number:@numerator_units[ :@original"0.1;
f0.10000000000000001 ��;@	:@denominator_units[ ;i�;o;	;"large-desktop;"large_desktop;@	;i�;@	;:
minus;i�");i�;)0;i�;*[[o;$;"
point;%"
point;@0;i