/******************************************************************************************

	This accordion plugin is designed to work with minimal markup. By default
	it is set up to use Definition Lists, but it's designed to work with any markup,
	as long as it has a container with a class of "accordion".

	If you want to use defferent markup you can specify the 'header' and 'content' selectors
	in the settings of the plugin.

	This plugin relies on jQuery.1.9.1.js.
	Tested and working in ie7+ and all good browsers

	If you require video in you accordion, please use the vMediaAccordion plugin instead.

	** To do... **
		Test with older version of jQuery
		Add CSS resets for other markup, ul/li's, H2's etc
		Give example of individual easing functions

*******************************************************************************************/

(function($) {
	$.fn.vAccordion = function(options) {

		var defaults = { // Plugin default options
			header: 'dt', // Default header selector
			content: 'dd', // Default content selector
			duration: 500, // Default animation duration
			easing: 'linear', // If you use anything other than this, you need to include the jquery easing plugin or individual parts of it.
			firstOpen: false // Is the first content area open? False by default.
		};

		options = $.extend(defaults, options); // Merge default and user defined options

		var	tP = parseInt($(options.content).css('padding-top'), 10),
			rP = parseInt($(options.content).css('padding-right'), 10),
			bP = parseInt($(options.content).css('padding-bottom'), 10),
			lP = parseInt($(options.content).css('padding-left'), 10),
			bBW = $(options.header).css('border-bottom-width'),
			bBS = $(options.header).css('border-bottom-style'),
			bBC = $(options.header).css('border-bottom-color'),
			border = bBW + ' ' + bBS+ ' ' + bBC; // Need to get these all seperately because ie7 and 8 doesn't understand just 'border-bottom'.

		return this.each(function() {
			var header = $(this).children(options.header); // Define header / click handler;
			$(this)
				.find(options.content)
				.each(function(){
					var h = $(this).height();
					$(this).data('originalHeight', h); // find each content block and store its height
				});
			header.click(onClick); // Initiate the onClick function
			header
				.css( 'cursor', 'pointer' ) // Give the header / click handler the appropriate cursor
				.next().each(resetContent); // Reset the next dom element - the content blocks
			haveEasing();
			if (options.firstOpen === true){ // If 'firstOpen' option is true, open the accordion item on load
				var firstChild = $(':first', this);
				showContent(firstChild);
				firstChild.addClass('active');
			}
		});

		function onClick() {
			if ( $(this).hasClass('active') ) { // Does this (header) have a class of 'active'
				hideContent(); // Initiate hideContent function
				$(this).removeClass("active"); // Remove class
			} else {
				hideContent(); // Initiate hideContent function
				showContent(this); // Initiate showContent function
				$(this).siblings(options.header).removeClass("active"); // Remove class
				$(this).addClass("active"); // Add class
			}
			return false; // Disable any default click events
		}

		function showContent(el){
			var h = $(el).next().data('originalHeight'); // Grab the value 'originalHeight' stored on this element
			$(el) // Apply css and animate the next dom element to this
				.next()
				.css({
					'border-bottom' : border // Put the border back if there already was one
				})
				.animate({
					height : h,
					'padding' : tP + ' ' + rP + ' '  + bP + ' ' + lP
			}, { duration: options.duration, easing: options.easing}); // Define duration and easing
		}

		function hideContent() {
			$('.active') // Hide current item with class of 'active'
			.next()
			.animate({
				height : 0,
				'padding' : 0 + ' ' + rP + ' ' + 0 + ' ' + lP
            }, { queue: false, duration: options.duration, easing: options.easing, complete: resetContent }); // Define queuing, duration, easing and apply resetContent function once complete
		}

		function resetContent() {
			$(this).css({
				'height' : 0,
				'overflow' : 'hidden',
				'border-bottom' : 'none',
				'display' : 'block',
				'padding' : 0 + ' ' + rP + 'px' + ' ' + 0 + ' ' + lP + 'px'
			}); // Reset CSS for the content area
		}

		function haveEasing() { // Warn the user if they have defined an easing property but not added the easing plugin
			if ((jQuery.easing["jswing"]) === undefined && options.easing !== 'linear'){
				console.log('*** You defined an easing property but not added the jQuery easing plugin. *** \n *** Download it here: http://gsgd.co.uk/sandbox/jquery/easing/ ***');
			}
		}

	};
})(jQuery);
