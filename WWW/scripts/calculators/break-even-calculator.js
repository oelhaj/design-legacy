/*

Calculator Template
Author: Dan V
Validation Requires jQuery Validation Plugin
http://docs.jquery.com/Plugins/Validation

*/


var UBP = UBP || {};

UBP.breakEvenCalc = UBP.breakEvenCalc || (function () {

	function calculate() {
		var form = document.getElementById('break-even-form'),
			valueEl = document.getElementById('value-be'),
			costEl = document.getElementById('cost-be'),
			expenditureEl = document.getElementById('expenditure-be'),
			numSales,
			numSalesEl = document.getElementById('number-sales-be');

		// console.log(settings.x);

		form.onsubmit = function (e) {
			if ($(this).valid()) {
				// Get the values...

				if (valueEl.value !== '') {
					valueElVal = valueEl.value;
				} else {
					valueElVal = 0;
				}

				if (costEl.value !== '') {
					costElVal = costEl.value;
				} else {
					costElVal = 0;
				}

				if (expenditureEl.value !== '') {
					expenditureElVal = expenditureEl.value;
				} else {
					expenditureElVal = 0;
				}

				// Do the math...
				// numSales = parseInt(expenditureElVal / (valueElVal-costElVal), 10);
				numSales = Math.ceil((expenditureElVal / (valueElVal - costElVal)));
				// Math.ceil(num * 10) / 10;

				// Assign the value
				if (typeof(numSales) === 'number'){
					numSalesEl.innerHTML = numSales;
				} else {
					numSalesEl.innerHTML = 0;
				}

				// numSalesEl.innerHTML = typeof(numSales);
			}

			// Prevent the form from submitting
			e.preventDefault();
		};

	}

	// Initialise allowing settings to be defined by the user
	function init(settingsObj) {
		settings = settingsObj;
		calculate(settings);
	}

	return {
		init: init
	};

}());

$(document).ready(function () {

	if ($('#break-even-form').length) {
		UBP.breakEvenCalc.init();

		$('#break-even-form').validate({
			rules: {
				'value-be-input': {
					required: true
				},
				'cost-be-input': {
					required: true
				},
				'expenditure-be-input': {
					required: true
				}

			},
			messages: {
				'value-be-input': {
					required: "Please enter the Value per sale"
				},
				'cost-be-input': {
					required: "Please enter the Cost per sale"
				},
				'expenditure-be-input': {
					required: "Please enter the Fixed expenditure"
				}
			}
		});
	}

});