/*

Calculator Template
Author: Dan V
Validation Requires jQuery Validation Plugin
http://docs.jquery.com/Plugins/Validation

*/


var UBP = UBP || {};

UBP.businessStartupCalc = UBP.businessStartupCalc || (function () {

	function calculate() {
		var form = document.getElementById('business-startup-form'),
			capitalEl = document.getElementById('capital-bs'),
			fundingRequirementEl = document.getElementById('funding-requirement-bs')

		// console.log(settings.x);

		form.onsubmit = function (e) {
			if ($(this).valid()) {
				// Add everything up except #capital-bs
				var total = 0;

				$(this).find('input[type=number]').not('#capital-bs').each(function () {
					
					var thisVal = parseInt($(this).val());

					if (!thisVal) {
						thisVal = 0;
					}

					total += thisVal;

				});

				// Minus the capital
				total = total - capitalEl.value;

				// console.log(total);

				// Insert total
				fundingRequirementEl.innerHTML = total;			}

			// Prevent the form from submitting
			e.preventDefault();
		};

	}

	// Initialise allowing settings to be defined by the user
	function init() {
		calculate();
	}

	return {
		init: init
	};

}());

$(document).ready(function () {

	UBP.businessStartupCalc.init();

	if ($('#business-startup-form').length) {

		$('#business-startup-form').validate({
			rules: {
				"capital-bs-input": {
					required: true
				}

			},
			messages: {
				"capital-bs-input": {
					required: "Please enter your Start-up Capital"
				}
			}
		});
	}

});