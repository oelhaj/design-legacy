/*

Calculator Template
Author: Dan V
Validation Requires jQuery Validation Plugin
http://docs.jquery.com/Plugins/Validation

*/


var UBP = UBP || {};

UBP.grossProfitCalc = UBP.grossProfitCalc || (function () {

	function calculate() {
		var form = document.getElementById('gross-profit-form'),
			valueEl = document.getElementById('value-gp'),
			costEl = document.getElementById('cost-gp'),
			grossProfit,
			grossProfitEl = document.getElementById('gross-profit-gp');

		// console.log(settings.x);

		form.onsubmit = function (e) {
			if ($(this).valid()) {
				// Get the values...

				if (valueEl.value !== '') {
					valueElVal = parseInt(valueEl.value,10);
				} else {
					valueElVal = 0;
				}

				if (costEl.value !== '') {
					costElVal = parseInt(costEl.value,10);
				} else {
					costElVal = 0;
				}
                
                var profit = valueElVal - costElVal;
                
                grossProfit = (profit / valueElVal) * 100;
                
				// console.log(grossProfit);

				// Assign the value
				grossProfitEl.innerHTML = grossProfit + '%';
			}

			// Prevent the form from submitting
			e.preventDefault();
		};

	}

	// Initialise allowing settings to be defined by the user
	function init(settingsObj) {
		settings = settingsObj;
		calculate(settings);
	}

	return {
		init: init
	};

}());

$(document).ready(function () {

	if ($('#gross-profit-form').length) {
		UBP.grossProfitCalc.init();

		$('#gross-profit-form').validate({
			rules: {
				'value-gp-input': {
					required: true
				},
				'cost-gp-input': {
					required: true
				}

			},
			messages: {
				'value-gp-input': {
					required: "Please enter the Average sales value"
				},
				'cost-gp-input': {
					required: "Please enter the Average cost of sale"
				}
			}
		});
	}

});