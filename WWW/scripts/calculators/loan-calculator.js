/*

Calculator Template
Author: Dan V
Validation Requires jQuery Validation Plugin with the additional method 'require_from_group' (see below)
http://docs.jquery.com/Plugins/Validation

 * Lets you say "at least X inputs that match selector Y must be filled."
 *
 * The end result is that neither of these inputs:
 *
 *  <input class="productinfo" name="partnumber">
 *  <input class="productinfo" name="description">
 *
 *  ...will validate unless at least one of them is filled.
 *
 * partnumber:  {require_from_group: [1,".productinfo"]},
 * description: {require_from_group: [1,".productinfo"]}
 *
 ***********/
jQuery.validator.addMethod("require_from_group", function(value, element, options) {
	var validator = this;
	var selector = options[1];
	var validOrNot = jQuery(selector, element.form).filter(function() {
		return validator.elementValue(this);
	}).length >= options[0];

	if(!$(element).data('being_validated')) {
		var fields = jQuery(selector, element.form);
		fields.data('being_validated', true);
		fields.valid();
		fields.data('being_validated', false);
	}
	return validOrNot;
}, jQuery.format("Please fill at least {0} of these fields."));
/*******/

var UBP = UBP || {};

UBP.loanCalc = UBP.loanCalc || (function () {

	var settings;

	function calculate() {
		var form = document.getElementById('loan-calculator-form'),
			loanAmountEl = document.getElementById('loan-amount-lc'),
			interestRateEl = document.getElementById('interest-rate-lc'),
			termYearsEl = document.getElementById('term-years-lc'),
			termMonthsEl = document.getElementById('term-months-lc'),
			termTotal,
			monthlyRepaymentEl = document.getElementById('monthly-repayment-lc'),
			interestPaidEl = document.getElementById('interest-paid-lc'),
			totalPaidEl = document.getElementById('total-paid-lc');

		// console.log(settings.x);

		form.onsubmit = function (e) {
			if ($(this).valid()) {
				// Get the values...
				loanAmountVal = loanAmountEl.value,
				interestRateVal = interestRateEl.value / 1200;

				if (termYearsEl.value !== '') {
					termYearsVal = termYearsEl.value;
				} else {
					termYearsVal = 0;
				}

				if (termMonthsEl.value !== '') {
					termMonthsVal = termMonthsEl.value;
				} else {
					termMonthsVal = 0;
				}

				// Do the math...
				termTotal = parseInt(termYearsVal * 12, 10) + parseInt(termMonthsVal, 10);
				monthlyRepaymentEl.innerHTML = (loanAmountVal * interestRateVal / (1 - (Math.pow(1 / (1 + interestRateVal), termTotal)))).toFixed(2);
				totalPaidEl.innerHTML = (monthlyRepaymentEl.innerHTML * termTotal).toFixed(2);
				interestPaidEl.innerHTML = (totalPaidEl.innerHTML - loanAmountVal).toFixed(2);
			}

			// Prevent the form from submitting
			e.preventDefault();
		};

	}

	// Initialise allowing settings to be defined by the user
	function init(settingsObj) {
		settings = settingsObj;
		calculate(settings);
	}

	return {
		init: init
	};

}());

$(document).ready(function () {

	if ($('#loan-calculator-form').length) {
		UBP.loanCalc.init({
			// x: 300
		});

		$('#loan-calculator-form').validate({
			rules: {
				'term-years-input': {
					require_from_group: [1, ".term-group"]
				},
				'term-months-input': {
					require_from_group: [1, ".term-group"]
				},
				'loan-amount-input': {
					required: true
				},
				'interest-rate-input': {
					required: true
				}

			},
			messages: {
				'loan-amount-input': {
					required: "Please enter your loan amount"
				},
				'interest-rate-input': {
					required: "Please enter your interest rate"
				}
			}
		});
	}

});