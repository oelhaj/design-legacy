/*

Calculator Template
Author: Dan V
Validation Requires jQuery Validation Plugin
http://docs.jquery.com/Plugins/Validation

*/


var UBP = UBP || {};

UBP.payslipCalc = UBP.payslipCalc || (function () {

	var settings;

	function calculate() {
		var form = document.getElementById('payslip-calculator-form'),
			grossPayEl = document.getElementById('gross-pay-psc'),
			weeklyMonthlyEl = document.getElementsByName('weekly-monthly-group'),
			weeklyMonthlyLen = weeklyMonthlyEl.length,
			numericalTaxCodeEl = document.getElementById('numerical-tax-code-psc'),
			taxCodesEl = document.getElementById('tax-codes-psc'),
			taxCodesVal;
			

		// console.log(settings.x);

		form.onsubmit = function (e) {
			//if ($(this).valid()) {
				var i = 0,
					weeklyMonthlyVal;
				// Get the values...
				grossPayVal = grossPayEl.value,
				numericalTaxCodeVal = numericalTaxCodeEl.value;

				// Figure out which radio button is selected
				for (i; i < weeklyMonthlyLen; i++) {
					if (weeklyMonthlyEl[i].checked) {
						weeklyMonthlyVal = weeklyMonthlyEl[i].value;
					}
				}

				// Figure out which option is selected
				taxCodesVal = taxCodesEl.options[taxCodesEl.selectedIndex].value;

				console.log(taxCodesVal);

				// Do the math...
			//}

			// Prevent the form from submitting
			e.preventDefault();
		};

	}

	// Initialise allowing settings to be defined by the user
	function init(settingsObj) {
		settings = settingsObj;
		calculate(settings);
	}

	return {
		init: init
	};

}());

$(document).ready(function () {

	if ($('#payslip-calculator-form').length) {
		UBP.payslipCalc.init({
			// x: 300
		});

		$('#payslip-calculator-form').validate({
			rules: {
				'weekly-monthly-group': {
					required: true
				},
				'gross-pay-input': {
					required: true
				},
				'numerical-tax-code-input': {
					required: true
				}

			},
			messages: {
				'weekly-monthly-group': {
					required: "Please select an option"
				},
				'gross-pay-input': {
					required: "Please enter your gross pay"
				},
				'numerical-tax-code-input': {
					required: "Please enter your numerical tax code"
				}
			}
		});
	}


});