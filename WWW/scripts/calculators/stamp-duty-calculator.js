/*

Calculator Template
Author: Dan V
Validation Requires jQuery Validation Plugin
http://docs.jquery.com/Plugins/Validation

*/


var UBP = UBP || {};

UBP.stampDutyCalc = UBP.stampDutyCalc || (function () {

	var settings;

	function calculate() {
		var form = document.getElementById('stamp-duty-calculator-form'),
			residentialEl = document.getElementsByName('residential-land-group'),
			residentialVal,
			amountEl = document.getElementById('consideration-amount-sdc'),
			amountVal,
			stampDutyPayableEl = document.getElementById('stamp-duty-payable-sdc'),
			stampDutyPayableVal;

		function doCalc(arr) {
			var i = 0,
			arrLen = arr.length;

			amountVal = amountEl.value;

			// Loop through all items in arr
			for (i; i < arrLen; i++) {
				// If (and hopefuly when) we land in a min / max range
				if (arr[i].rangeMin <= amountVal && arr[i].rangeMax >= amountVal) {
					stampDutyPayableVal = (arr[i].percent / 100) * amountVal;
					//console.log(stampDutyPayableVal);
				} else if (arr[i].rangeMin <= amountVal) {
					stampDutyPayableVal = (arr[i].percent / 100) * amountVal;
				}
			}
		}

		function getRadioButtonValue(el) {
			var i = 0,
				radioLen = el.length,
				radioVal;
			// Figure out which radio button is selected
			for (i; i < radioLen; i++) {
				if (residentialEl[i].checked) {
					radioVal = el[i].value;
				}
			}
			residentialVal = radioVal;
		}

		form.onsubmit = function (e) {
			if ($(this).valid()) {
				// Get Radio Button Value
				getRadioButtonValue(residentialEl);

				if (residentialVal === 'residential-land') {
					doCalc(settings.residential);
					stampDutyPayableEl.innerHTML = (stampDutyPayableVal).toFixed(2); // Assign the answer
				} else if (residentialVal === 'non-residential-land') {
					doCalc(settings.nonResidential);
					stampDutyPayableEl.innerHTML = (stampDutyPayableVal).toFixed(2); // Assign the answer
				}
			}

			// Prevent the form from submitting
			e.preventDefault();
		};

	}

	// Initialise allowing settings to be defined by the user
	function init(settingsarr) {
		settings = settingsarr;
		calculate(settings);
	}

	return {
		init: init
	};

}());

$(document).ready(function () {

	UBP.stampDutyCalc.init({
		residential: [
			{
				rangeMin: 0,
				rangeMax: 125000,
				percent: 0
			},
			{
				rangeMin: 125001,
				rangeMax: 250000,
				percent: 1
			},
			{
				rangeMin: 250001,
				rangeMax: 500000,
				percent: 3
			},
			{
				rangeMin: 500001,
				rangeMax: 1000000,
				percent: 4
			},
			{
				rangeMin: 1000001,
				rangeMax: 2000000,
				percent: 5
			},
			{
				rangeMin: 2000001,
				rangeMax: 0,
				percent: 7
			}
		],
		nonResidential: [
			{
				rangeMin: 0,
				rangeMax: 150000,
				percent: 0
			},
			{
				rangeMin: 150001,
				rangeMax: 250000,
				percent: 1
			},
			{
				rangeMin: 250001,
				rangeMax: 500000,
				percent: 3
			},
			{
				rangeMin: 500001,
				rangeMax: 0,
				percent: 4
			}
		]
	});

	$('#stamp-duty-calculator-form').validate({
		rules: {
			'residential-land-group': {
				required: true
			},
			'consideration-amount-input': {
				required: true
			}

		},
		messages: {
			'residential-land-group': {
				required: "Please select an option"
			},
			'consideration-amount-input': {
				required: "Please enter your amount"
			}
		}
	});


});