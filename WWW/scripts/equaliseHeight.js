// Equalise button height in button lists
function equaliseHeight() {
	function equalHeight(group) {
		tallest = 0;
		group.each(function () {
			thisHeight = $(this).height();
			if (thisHeight > tallest) {
				tallest = thisHeight;
			}
		});
		group.height(tallest);
	}
	equalHeight($(".homepage .section-top .teaser .text"));
	equalHeight($(".homepage .section-middle .teaser .text"));
	equalHeight($("#footer .aside .widget-container > .module .regular:not(.regular .regular)"));
}

// Call functions on doc ready
$(document).ready(function () {
	equaliseHeight();
});