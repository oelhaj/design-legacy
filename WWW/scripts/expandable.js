UBP.expandableContent = UBP.expandableContent || (function () {

	// Expandable Information (Hide and show content block on toggle click)
	
	function doExpand () {
		var $expandable = $('.expandable').has($(".toggle")),
			$expandableToggle = $expandable.find('.toggle'),
			$expandableContent = $expandable.find('.content'),
			expandedStatus;
		
		function openContent(animation) {
			if (animation == 'slide') {
				$expandableContent.stop().slideDown();
			} else if (animation == 'fade') {
				$expandableContent.stop().fadeIn();
			} else {
				$expandableContent.css('display', 'block');
			}
			$expandable.addClass('open').removeClass('close');
			expandedStatus == true;
		}
		
		function closeContent(animation) {
			if (animation == 'slide') {
				$expandableContent.stop().slideUp();
			} else if (animation == 'fade') {
				$expandableContent.stop().fadeOut();
			} else {
				$expandableContent.css('display','none');
			}
			$expandable.addClass('close').removeClass('open');
			expandedStatus == false;
		}
		
		$expandable.each(function () {
			$expandable = $(this);
			$expandableContent = $expandable.find('.content');
			
			if ($expandable.hasClass('open')) {
				openContent();
			} else if ($expandable.hasClass('close')) {
				closeContent();
			} else {
				closeContent();
			};
		});
	
		$expandableToggle.click(function (e) {
			var $this = $(this);
			e.preventDefault();
			$expandable = $this.parents('.expandable').has($(".toggle"));
			$expandableToggle = $expandable.find('.toggle');
			$expandableContent = $expandable.find('.content');
			
			if ($expandable.hasClass('open')) {
				closeContent('slide');
			} else if ($expandable.hasClass('close')) {
				openContent('slide');
			}
		});
	}
	
	return {
		init: doExpand
	};
}());

$(document).ready(function () {
	UBP.expandableContent.init();
});
