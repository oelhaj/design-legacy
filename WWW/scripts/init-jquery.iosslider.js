var UBP = UBP || {};
UBP.carousel = (function () {

	var $container = $('.slider'),
		tally = 0, // number of images loaded/failed to load
		sliderOptions,
		carouselFragment,
		$slideLinks, // jQuery object holding the pagination buttons for indiviual slides
		showSlideLinks;

	/**		
		Expected format of image array.
		images: [
				{url: "/path/to/image.jpg", alt: "Descriptive text"},
				{url: "http://path/to/image.jpg", alt: "Descriptive text"}
			]
	*/

	function createCarouselFragment(carouselImgList) {
		var li,
			textContent,
			graphicDiv,
			img,
			heroHeading,
			heroText,
			textDiv,
			i,
			numImgs = carouselImgList.length;

		carouselFragment = document.createDocumentFragment();

		for (i = 0; i < numImgs; i += 1) {

			li = document.createElement('li'); // Create li
			li.className = 'slide';

			img = document.createElement('img'); // Create Image
			img.id = "img-" + (i + 1); // Give it an ID
			img.src = carouselImgList[i].url; // Add the Source
			img.alt = carouselImgList[i].alt; // And the Alt tag

			graphicDiv = document.createElement('div'); // Create Graphic Div
			graphicDiv.className = 'graphics'; // Add a class
			graphicDiv.appendChild(img); // Add the image

			li.appendChild(graphicDiv); // add the graphics div and all its content

			// Create heading container
			if (carouselImgList[i].heroHeading !== '' || carouselImgList[i].heroText !== '') { // If Any text exsists
				textDiv = document.createElement('div'); // Create Text Div
				textDiv.className = 'text'; // Add a class
			}

			// create heading
			if (carouselImgList[i].heroHeading !== '') { // If Heading exsists
				heroHeading = document.createElement('h2'); // Create H2
				heroHeading.textContent = carouselImgList[i].heroHeading; // Define its content	
				textDiv.appendChild(heroHeading); // Append it to div
			}

			// create sub heading
			if (carouselImgList[i].heroText !== '') { // If Sub Heading exsists
				heroText = document.createElement('p'); // Create P
				heroText.textContent = carouselImgList[i].heroText; // Define its content
				textDiv.appendChild(heroText); // Append it to div
			}

			// Add headings to li
			if (carouselImgList[i].heroHeading !== '' || carouselImgList[i].heroText !== '') { // If Any text exsists
				li.appendChild(textDiv); // add the text div and all its content
			}

			if (carouselImgList[i].Quote === true) {
				li.className += ' quote';
			}

			carouselFragment.appendChild(li); // add the li and all its content to the docfrag

			//console.log('#' + img.id + ' loaded!');
		}

		insertImages();
		initSlider();
		//console.log(carouselFragment);
	}

	function insertImages() {
		$container.append(carouselFragment);
		//imageLoaded(i);
	}

	function initSlider() {
		$container.animate({
			opacity: 1
		});
		$('.prev-button, .next-button').wrapAll('<div class="pagination" />').fadeIn();
		$('.loading-icon').fadeOut();
		$('.loading-css3').fadeOut();
		$('.iosSlider').iosSlider(sliderOptions);
		if (showSlideLinks === true) {
			$('.pagination .prev-button').after($slideLinks);
			$slideLinks.css({visibility: 'visible', display: 'none'});
			$slideLinks.delay(500).fadeIn(1000);
		} else {
			$slideLinks.remove();
		}
	}

	function updatePagination(args) {
		var $paginationListItems,
			currentSlideIndex,
			$currentSlidePagination,
			activeClassName;

		if (!showSlideLinks) {
			return;
		}

		$paginationListItems = $('.pagination .slide-link-item');
		currentSlideIndex = args.currentSlideNumber - 1;
		$currentSlidePagination = $($paginationListItems[currentSlideIndex]);
		activeClassName = 'active';
		
		$paginationListItems.removeClass(activeClassName);
		$currentSlidePagination.addClass(activeClassName);
	}

	function init(settingsObj) {
		sliderOptions = settingsObj.sliderOptions;
		$slideLinks = settingsObj.slideLinks;
		showSlideLinks = settingsObj.showSlideLinks;
		createCarouselFragment(settingsObj.images);
	}

	return {
		init: init,
		updatePagination: updatePagination
	};

} ());


$(window).load(function () {
	var $slideLinks = $('.slide-link-list'), // div to hold the 1, 2, 3.. slide pagination buttons
		showSlideLinks = true; // boolean: if false removes the cshtml rendered element

	UBP.carousel.init({
		// UBP.carouselSettings is populated by HeroImage.cshtml
		images: UBP.carouselSettings || [],
		slideLinks: $slideLinks, // jQuery object holding the pagination buttons for indiviual slides
		showSlideLinks: showSlideLinks, // boolean: if false removes the cshtml rendered element
		// These options can be anything that iOS slider accepts.
		sliderOptions: {
			snapToChildren: true,
			snapSlideCenter: true,
			desktopClickDrag: true,
			keyboardControls: true,
			infiniteSlider: true,
			navNextSelector: $('.next-button'),
			navPrevSelector: $('.prev-button'),
			navSlideSelector: $('.slide-link-item'),
			onSlideComplete: UBP.carousel.updatePagination
		}
	});
});
