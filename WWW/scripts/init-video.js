$(window).load(function () {
	// set up videos
	if ($("object").length > 0 || $("video").length > 0) {
		MEDIA_CONTROL.init({
			swfPrefs: {
				// swfPath: '/flash/player.swf',	// overrides default
				// bg: '996600',
				// hi: '000000',
				a: '0.8'
				// bigPlayX: '250',	// if either X or Y are ommitted the big play button will be centred
				// bigPlayY: '500',	// if either X or Y are ommitted the big play button will be centred
				// bigPlayWidth: '150',
				// bigPlayHeight: '150'
			}
		});
	}
});