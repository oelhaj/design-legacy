(function ($) {
	$.fn.multiLocationGoogleMap = function (options, data) {

		var defaults = { // Plugin default options
				maxZoomOnLoad: 17
			},
			winWidth = window.innerWidth,
			latLngs = [],
			map,
			latLngBounds,
			infowindow,
			service,
			request,
			imgTag,
			isWidget;


		options = $.extend(defaults, options); // Merge default and user defined options


		// Initialise the Google Map...
		return this.each(function () {
			var $staticMapsWrapper;

			isWidget = $(this).parents('.widget').length;
			// if there is data...
			if (options.CMSData.Markers.length) {
				if (winWidth > 480) {
					google.maps.event.addDomListener(window, 'load', initialize(this)); // Interactive map
				} else {
					$staticMapsWrapper = $('.m-map .graphics');
					if ($('.m-map .graphics img').length === 0) {
						imgTag = new Image();
						imgTag.src = UBP.gImgSrc;
						$staticMapsWrapper.append(imgTag); // Static map for mobiles / small screens
						$('.gMap').remove();
					}
				}
			}
		});

		function initialize(e) {

			var mapOptions = {
					zoom: 4,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					mapTypeControl: false,
					streetViewControl: false
				};

			// Update vars...
			map = new google.maps.Map(document.getElementById(e.id), mapOptions),
			latLngBounds = new google.maps.LatLngBounds(),
			infowindow = new google.maps.InfoWindow();
			// CMSData = UBP.mapData,
			// request = {
			// reference: 'CnRkAAAAGnBVNFDeQoOQHzgdOpOqJNV7K9-c5IQrWFUYD9TNhUmz5-aHhfqyKH0zmAcUlkqVCrpaKcV8ZjGQKzB6GXxtzUYcP-muHafGsmW-1CwjTPBCmK43AZpAwW0FRtQDQADj3H2bzwwHVIXlQAiccm7r4xIQmjt_Oqm2FejWpBxLWs3L_RoUbharABi5FMnKnzmRL2TGju6UA4k'
			// },
			// service = new google.maps.places.PlacesService(map),
			// service.search(request, callback);
			/////
			setMarkers();
		}

		function callback(place, status) {
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				createMarker(place);
			}
		}

		function setMarkers() {

			$.each(options.CMSData.Markers, function (i, m) {
				var myLatLng = new google.maps.LatLng(m.Latitude, m.Longitude),
					iconBase = '/img/icons/',
					iconShadow = {
						url: iconBase + 'bg-map-marker-shadow.png',
						anchor: new google.maps.Point(14, 39)
					};

				latLngs[i] = myLatLng;
				marker = new google.maps.Marker({
					position: myLatLng,
					map: map,
					icon: iconBase + 'bg-map-marker-active.png',
					shadow: iconShadow
				});

				// Add click functionality to the markers
				if (!isWidget) {
					google.maps.event.addListener(marker, 'click', (function (marker, i) {
						return function () {
							infowindow.setContent(m.Name);
							infowindow.open(map, marker);
						};
					})(marker, i));
				}
			});

			// Define the boundaries
			$.each(latLngs, function (n) {
				latLngBounds.extend(this);
			});

			// Set the map center and zoom level based on all the markers 
			map.setCenter(latLngBounds.getCenter());
			map.fitBounds(latLngBounds);

			// on load restrict the map from zooming in too far
			var listener = google.maps.event.addListener(map, "idle", function () {
				if (map.getZoom() > options.maxZoomOnLoad) map.setZoom(options.maxZoomOnLoad);
				google.maps.event.removeListener(listener);
			});

			// Adjust the center position on win resize
			$(window).resize(function () {
				window.setTimeout(function () {
					map.panTo(latLngBounds.getCenter());
				}, 1000);
			});
		}

	};
})(jQuery);