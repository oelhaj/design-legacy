var UBP = UBP || {};

UBP.common = UBP.common || (function () {

	function loadAsync(url) {
		var g = document.createElement('script'),
			s = document.getElementsByTagName('script')[0];
		g.src = url;
		s.parentNode.insertBefore(g, s);
	}

	function init() {

		if ($('.icon-share').length > 0) {
			loadAsync('http://s7.addthis.com/js/250/addthis_widget.js');
		}
	}

	return {
		init: init,
		loadAsync: loadAsync
	};

}());

UBP.mobileNav =  (function () {

/* This will only work for 2 navigation tiers */

	var $menuToggle = $('.navigation.primary .navigation-toggle'),
		$menu = $('.navigation.primary > ul'),
		$menuItem = $('.navigation.primary > ul li a'),
		winWidth = $(window).width(),
		breakPoint = 480, // Update this if the mobile breakpoint ever changes
		defaults = {
			speed: 250,
			openMenu: false,
			nth: 1,
			headerLinks: false // If false and if top level item has children, this will be disabled on first click, allowing its children to display. Second click will navigate.
		},
		options = {};

	function init(initObj) {

		options = $.extend(defaults, initObj);

		// show / hide top level mobile nav
		$menuToggle.on('click', function (e) {
			e.preventDefault();
			if ($(this).hasClass('expanded')) {
				$menu.slideUp(options.speed);
				$(this).removeClass('expanded').html('Menu');
			} else {
				$menu.slideDown(options.speed);
				$(this).addClass('expanded').html('Close');
			}
		});

		// Show / hide second level
		if (winWidth < breakPoint) {
			if (options.headerLinks === false) {
				$menuItem.click(function (e) {
					$('li a.expanded').not(this).removeClass('link-active expanded').next('ul').slideUp(options.speed); // Slide up other open child UL's
					if ($(this).next('ul').children('li').length > 0 && !$(this).hasClass('link-active')) { // if children and not active
						e.preventDefault(); // disable navigation
						$(this).addClass('link-active expanded').next('ul').slideDown(options.speed); // Show children
					}
				});
			}
		}

		// On resize hide any children
		window.onresize = function (event) {
			winWidth = $(window).width(); // set the var on resize
			if (winWidth > breakPoint) {
				$('li a.expanded').removeClass('expanded link-active').next('ul').hide(); // Hide nav if NOT mobile and remove classes
				$menu.show(); // show top level nav if more
			}
		};

/*
		var $form = $('#registration-form');
		$form.submit(function () {

			var $formData = $form.serialize();
			console.log($formData);

			$.ajax({
				type: 'post',
				url: window.location.pathname,
				data: $formData,
				processData: false,
				crossDomain: true,
				// async: true,
				// cache: false,
				// contentType: "application/json; charset=utf-8",
				// dataType: "jsonp",
				success: function (data) {
					console.log('ok');
					return true;
				},
				error: function (request, status, error)
				{
					console.log('Oops, there seems to be an issue. Please try again.');
				}
			});
			event.preventDefault();
		});
*/
	}

	return {
		init: init
	};

} ());

UBP.clearField = UBP.clearField || (function () {

	// field clearing and highlight functionality

	function doClearField(formfield) {

		var $searchText = formfield.attr('placeholder');
		formfield.addClass('inactive');
		if (formfield.val() === '') {
			formfield.addClass('blank');
		}

		formfield.focus(function () {
			formfield.removeClass('inactive').removeClass('blank');
			formfield.attr('placeholder', '');

		}).blur(function () {
			formfield.addClass('inactive');
			if (formfield.val() === '') {
				formfield.attr('placeholder', $searchText);
				formfield.addClass('blank');
			}
		});
	}

	return {
		init: doClearField
	};

}());

UBP.touchHover = UBP.touchHover || (function () {

	// Disable initial click and allow touch as hover state on navigation

	function doStopClick() {
		var navItems = $('.navigation.primary > ul > li:has("ul")');
		
		//Detect Android		
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
		
		//Stop initial click follow through on Android
		if (isAndroid) {		
			navItems.each(function() {
				var $this = $(this),
						count = 0,
						navLink = $this.find('> a');
						
				navLink.on('click', function (e) {
					if (count < 1) {
						e.preventDefault();
						count++;
					}
				});
			});
		}
	}

	return {
		init: doStopClick
	};

}());

$(document).ready(function () {
	UBP.common.init();
	UBP.mobileNav.init();
	UBP.clearField.init($('.m-search-form form input[placeholder]'));
	UBP.touchHover.init();
});
