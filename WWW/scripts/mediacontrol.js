/*
MEDIA_CONTROL
v 2.1

A central object with which to control all HTML5 video + Flash fallback on the page.
Depends on 
	swfobject
	html5shim/modernizr (for IE only).


IMPORTANT:
==========
PUT THE player.swf IN THE SAME FODLER AS THIS JS FILE FOR DEFAULT BEHAVIOUR AND MINIMAL CONFIG OBJ.

initObj example:
	{
		swfPrefs: {
			swfPath: '/flash/player.swf',	// overrides default
			bg: '996600',					// hex value of bgs - note no # at start
			hi: '000000',					// hex value of highlights/text - note no # at start
			a: '0.5',						// default alpha of elements
			bigPlayX: '250',				// if either X or Y are ommitted the big play button will be centred
			bigPlayY: '500',				// if either X or Y are ommitted the big play button will be centred
			bigPlayWidth: '150',
			bigPlayHeight: '150'
		}
	}

WORKING VERSION AT 
http://staging.tmpw.co.uk/PROJECTS/TEST/MediaControl/


Markup:
=======
Use Modernizr to ensure <video> is rendered in older browsers.
<video id="a-video-element" width="624" height="352" controls poster="media/marketing.jpg">
<!-- MP4 must be first for iOS -->
<source src="media/marketing.mp4" type="video/mp4" />
<source src="media/marketing.ogv" type="video/ogg" />
<div class="video-fallback-image">
<img src="media/marketing.jpg" width="624" height="352" alt="__TITLE__" />
</div>
</video>

<ul>
<li><button id="play_btn" rel="a-video-element">PLAY</button></li>
<li><button id="pause_btn" rel="a-video-element">PAUSE</button></li>
<li><button id="src_btn">NEW SOURCE</button></li>
</ul>

id needed on <video>
use <source> tags even if there is only one source with type attribute
Put width and height attributes on <video>
Put in fallback content (class="video-fallback-image" is optional).

For the buttons, ids are required. rel is handy for play and pause, but could be added to MEDIA_CONTROL.play() call dynamically.

Usage:
======
MEDIA_CONTROL.init(initObj);

then
MEDIA_CONTROL.play('a-video-element');
MEDIA_CONTROL.pause('a-video-element');
MEDIA_CONTROL.seek('a-video-element', 20);
MEDIA_CONTROL.setSources('a-video-element', 'url/to/posterframe', [{src:'/url/to/next-vid.mp4', type:'video/mp4'}, {src:'/url/to/next-vid.ogv', type:'video/ogg'}, {src:'/url/to/next-vid.webm', type:'video/webm'}]);

If using one player to view multiple videos (jukebox style) then all movies must be encoded with all the source codecs that the first movie has.
EG if first movie has MP4 and OGV then FF will go HTML5 native on init. If the other movies only have MP4s, then the player won't reconfigure itself into Flash video and the MP4 won't play.

Also all movies must be the same dimensions.


Refs:
http://dev.opera.com/articles/view/everything-you-need-to-know-about-html5-video-and-audio/



Version Log:
============
2012-04-05
DJL
	Made flashVidUrlValidator() more robust esp when IE is not supplied absolute urls.

2012-03-07
DJL
	Custom flash player plus customisation via js.
	Much smaller swf payload.
	swf now sits in same place as mediacontrol.js
	should be no need to delve into mediacontrol.js to alter paths
	improved error handling in swf

======
24.01.2012 - P.Young
Description: Added an extra check in flashVidUrlValidator() to strip any cache refresh query strings (.ie ?v=2) as this breaks the flash call for the video.


 */
var MEDIA_CONTROL = (function () {
    
	var vidEls = [], // holds original <video> elements at page load
		playersObject = {}, // holds <video> elements or swf objects in named keys to be referenced later
		playersArray = [], // holds <video> elements or swf objects in numbered array indicies
		mode = '', // will be one of ['html5', 'flash', 'none']
		swfPluginVersion = "10",
		flashParams = {
		menu : "false",
		scale : "noScale",
		allowFullscreen : "true",
		allowScriptAccess : "always",
		wmode : "opaque"
		},
		flashAttrs = {id: 'video', name: 'video'},
		flashVars = {},
		swfPrefs = {},
		initialMP4Src = '',
		initialOGVSrc = '',
		initialWebMSrc = '',
		// get this script's path - make sure this is run when the html page executes
		scripts = document.getElementsByTagName('script'),
		path = scripts[scripts.length-1].src.split('?')[0], // remove any ?query
		mydir = path.split('/').slice(0, -1).join('/')+'/',
		swfURL = mydir + 'player.swf';

    // exposed to public API
    function play(id) {
      switch (mode) {
      case 'html5':
        playersObject[id].play();
        break;
        
      case 'flash':
	  if (playersObject[id].playVid === undefined) {
			//console.log('swf playVid is undefined');
	  } else {
        playersObject[id].playVid();
	  }
      break;
        
      default:
      }
    }
    
    // exposed to public API
    function pause(id) {
      switch (mode) {
      case 'html5':
        playersObject[id].pause();
        break;
        
      case 'flash':
        playersObject[id].pauseVid();
        break;
        
      default:
      }
    }
    
    // exposed to public API
    function seek(id, seekTime) {
      switch (mode) {
      case 'html5':
		//console.log('playersObject[id]: ' + playersObject[id]);
		try {
			playersObject[id].currentTime = seekTime;
		} catch (e) {}
        break;
        
      case 'flash':
	  try {
        playersObject[id].seekVid(seekTime);
	} catch (e) {}
        break;
        
      default:
      }
    }
    
    // called by refreshHTML5Sources()
    // srcObj example: {src:'media/technology.mp4', type:'video/mp4'}
    function createSource(srcObj) {
      var newSourceEl,
        attrSrc,
        attrType;
      
      newSourceEl = document.createElement('source');
      
      attrSrc = document.createAttribute('src');
      attrSrc.value = srcObj.src;
      newSourceEl.setAttributeNode(attrSrc);
      
      attrType = document.createAttribute('type');
      attrType.value = srcObj.type;
      newSourceEl.setAttributeNode(attrType);
      
      return newSourceEl;
    }
    
    // called by setSources() in html5 mode
    // thisPlayer is a <video> element
    function refreshHTML5Sources(thisPlayer, sourceArray) {
      var sources,
        len,
        i;
      
      // if you don't pause it can crash the video element and the browser controls won't show up
      thisPlayer.pause();
      
      // clear out the <video>s current sources
      sources = thisPlayer.getElementsByTagName('source');
      len = sources.length;
      for (i = 0; i < len; i++) {
        // keep removing the first element
        thisPlayer.removeChild(sources[0]);
      }
      
      // create new video sources and append them to <video>
      len = sourceArray.length;
      for (i = 0; i < len; i++) {
        thisPlayer.appendChild(createSource(sourceArray[i]));
      }
    }
    
    function flashVidUrlValidator(rawURL) {
		var retVal = rawURL,
			split,
			lastSlash;
			
		// If a query string is being used to force a cache refresh, remove it from the url first
		split = retVal.split('?');
		retVal = split[0];
		
		// IE doesn't reliably pass the absolute URL to the SWF
		if (retVal.match(/^http/)) {
			// given media url is already an absolute url
			return retVal;		
		} else if (retVal.charAt(0) === '/') {
			// given media url is relative to site root
			return window.location.protocol + '//' + window.location.hostname + retVal;
		} else if (window.location.pathname.match(/\/$/)) {
			// given media url is not absolute and is not relative to site root
			// but current path ends with a slash, 
			// so we're in a folder and the media url is relative to that folder
			return window.location.protocol + '//' + window.location.hostname + window.location.pathname + retVal;		
		} else {
			// given media url is not absoltue and is not relative to site root
			// and current path does not end with a slash, 
			// so assume the media url is relative to current path's containing folder
			lastSlash = window.location.pathname.lastIndexOf('/');
			return window.location.protocol + '//' + window.location.hostname + window.location.pathname.slice(0, lastSlash) + '/' + retVal;
		}
	}
    
    // exposed to public API
    function setSources(id, posterURL, sourceArray) {
      var thisPlayer = playersObject[id],
        MP4Url = '',
        len,
        i,
		swfParentEl,
		attrID,
		newSwfContainer;
      
      switch (mode) {
      case 'html5':
        thisPlayer.poster = posterURL;
        refreshHTML5Sources(thisPlayer, sourceArray);
        thisPlayer.load();
        break;
        
      case 'flash':
        // get MP4 URL
        len = sourceArray.length;
        for (i = 0; i < len; i++) {
          if (sourceArray[i].type === 'video/mp4') {
            MP4Url = sourceArray[i].src;
            break;
          }
        }
        
		if (thisPlayer.setVidSource === undefined) {
			console.log('thisPlayer.setVidSource is undefined');
		} else {
			log('flash vid source');
			thisPlayer.setVidSource(posterURL, flashVidUrlValidator(MP4Url));
		}
        break;
        
      default:
      }
      
    }
    
    // e.id, String indicating the ID used in swfobject.registerObject
    // e.ref, HTML object element reference (returns undefined when success=false)
    // add the html element to playersObject and playersArray in this swfobject callback otherwise dom is not ready in time
    function rememberSwf(e) {
      // Fix SWFObject ExternalInterface bug on IE when swf is within a form node
      // http://alincostin.com/index.php?option=com_content&view=article&id=64:swfobject-externalinterface-bug-on-ie&catid=36:general-javascript-category&Itemid=53
      if (document.forms.length > 0 && document.forms[0].contains && document.forms[0].contains(e.ref) === true) {
        window[e.id] = e.ref;
      }
      playersObject[e.id] = e.ref;
      playersArray.push(e.ref);
    }
    
    function setupFlashVidPlayer(targetId) {
      var videoElement = document.getElementById(targetId),
        playerWidth = videoElement.getAttribute('width'),
        playerHeight = videoElement.getAttribute('height');
      
		flashAttrs.id = flashAttrs.name = targetId;
		flashVars.posterURL = videoElement.getAttribute('poster');
		flashVars.mediaURL = flashVidUrlValidator(initialMP4Src);
		flashVars.width = playerWidth;
		flashVars.height = playerHeight;
		flashVars.skinBackground = 	swfPrefs.bg 			|| '000000';
		flashVars.skinHighlight = 	swfPrefs.hi 			|| 'FFFFFF';
		flashVars.skinAlpha = 		swfPrefs.a 				|| '0.5';
		flashVars.bigPlayX = 		swfPrefs.bigPlayX 		|| '';
		flashVars.bigPlayY = 		swfPrefs.bigPlayY 		|| '';
		flashVars.bigPlayWidth = 	swfPrefs.bigPlayWidth 	|| '';
		flashVars.bigPlayHeight = 	swfPrefs.bigPlayHeight 	|| '';

		swfobject.embedSWF(swfURL, targetId, playerWidth, playerHeight, swfPluginVersion, false, flashVars, flashParams, flashAttrs, rememberSwf);
    }
    
    // override this in parent script for future extensibility...
    function setUpHTML5Player(videoElement) {}
    
    // override this in calling script for future extensibility...
    function noPlayback(videoElement) {}
    
	
    function init(initObj) {
      
      var sources,
        canPlayMP4,
        canPlayOGV,
        canPlayWebM,
        sourceHasMP4,
        sourceHasOGV,
        sourceHasWebM,
        numSources,
        i,
        j,
        thisType,
        thisVidId,
        thisSWF;
      
      // establish mode
      mode = 'none';
      vidEls = document.getElementsByTagName('video');
      canPlayMP4 = '';
      canPlayOGV = '';
      canPlayWebM = '';
      if (vidEls[0].canPlayType) {
        canPlayMP4 = vidEls[0].canPlayType('video/mp4');
        canPlayOGV = vidEls[0].canPlayType('video/ogg');
        canPlayWebM = vidEls[0].canPlayType('video/webm');
      }
      
      // iterate backwards through nodes as vidEls is a live list
      i = vidEls.length;
      while (i--) {
        // initialise vars and work out what source files we're dealing with
        sourceHasMP4 = false;
        sourceHasOGV = false;
        sourceHasWebM = false;
        sources = vidEls[i].getElementsByTagName('source');
        numSources = sources.length;
        for (j = 0; j < numSources; j++) {
          thisType = sources[j].getAttribute('type');
          if (thisType.search('video/mp4') > -1) {
            sourceHasMP4 = true;
            initialMP4Src = sources[j].src;
          } else if (thisType.search('video/ogg') > -1) {
            sourceHasOGV = true;
            initialOGVSrc = sources[j].src;
          } else if (thisType.search('video/webm') > -1) {
            sourceHasWebM = true;
            initialWebMSrc = sources[j].src;
          }
        }
        
        // mode depends on what capabilities the browser has and the sources available
        // use .length > 2 as early html5 spec had 'no' as a valid value
        if (sourceHasMP4 && canPlayMP4.length > 2) {
          mode = 'html5'; //html5
        } else if (sourceHasOGV && canPlayOGV.length > 2) {
          mode = 'html5';
        } else if (sourceHasWebM && canPlayWebM.length > 2) {
          mode = 'html5';
        } else if (sourceHasMP4 && swfobject.hasFlashPlayerVersion(swfPluginVersion)) {
          mode = 'flash';
        }
		
		if (initObj !== undefined) {
			swfPrefs = initObj.swfPrefs || {};
			if (swfPrefs.swfPath !== undefined && swfPrefs.swfPath !== '') {
				swfURL = swfPrefs.swfPath;
			}
		}
		
        // set up players and add them to playersObject and playersArray
        thisVidId = vidEls[i].getAttribute('id');
        switch (mode) {
        case 'html5':
          setUpHTML5Player(); // parent script can override this function for future extensibility
          playersObject[thisVidId] = document.getElementById(thisVidId);
          playersArray.push(document.getElementById(thisVidId));
          break;
          
        case 'flash':
          // adds the html element to playersObject and playersArray in swfobject callback
          setupFlashVidPlayer(thisVidId);
          break;
          
        case 'none':
          noPlayback(vidEls[i]);
          break;
          
        default:
          noPlayback(vidEls[i]);
        }
        
      }
      //alert('init complete, mode is: ' + mode);
      //log(playersObject['a-video-element']);
      //log(playersArray[0]);
      //playersArray[1].style.opacity = "0.5";
    }
	
    /*
    function initWithDelay() {
      // needs a delay for IE
      setTimeout(initAfterDelay, 50);
    }
    */

    return {
      // init: initWithDelay,
      init : init,
      //initAfterDelay: initAfterDelay,
      play : play,
      pause : pause,
      seek : seek,
      setSources : setSources
    };
  }());
 