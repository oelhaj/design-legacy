var UBP = UBP || {};

UBP.navigationAccordion = UBP.navigationAccordion || (function () {

	// secondary accordion menu with options - add accordion class to parent container for this to kick  in
	var accordionSlider = (function () {
	
	    var $navigation = $('.navigation.accordion'),
	    		$childNav = $navigation.find('ul li ul'),
	        $navHeader = $navigation.find('ul:first > li'),
	        $navLinks = $navHeader.filter('a'),
	        $nestedChildren = $navHeader.children('ul'),
	        defaults = {
	            speed: 500,
	            openMenu: false,
	            nth: 1,
	            headerLinks: false
	        },
	        options = {};
	      
	    function slider(initObj) {
	
	        options = $.extend(defaults, initObj);
	
	        $childNav.addClass('hidden-menu');
	
	        if (options.openMenu === true) {
	            $navHeader.eq(options.nth).addClass('expanded').find($childNav).removeClass('hidden-menu');
	        } else if (options.openMenu === true) {
	            $navHeader.eq(options.nth).addClass('expanded').find($childNav).removeClass('hidden-menu');
	        }
	
	        if (options.headerLinks === false && $navHeader.hasClass('has-child')) {
	            $navLinks.click(function (e) {
	                e.preventDefault();
	            });
	        }
	
	        // checks for child UL elements if non then removes the background arrow from the span element
	        if ($nestedChildren.length >= 1) {
	            $navHeader.filter('li:has(> ul > li)').addClass('has-child');
	        }
	
	        $navHeader.click(function (e) {
	            if (!$(this).hasClass('expanded') && $(this).hasClass('has-child')) {
	                e.preventDefault();
	            }
	
	            if (options.headerLinks === false && $(this).hasClass('expanded')) {
		          		$(this).removeClass('expanded').find($childNav).slideUp(options.speed);
		          		e.preventDefault();
	            } else {
									$(this).addClass('expanded').find($childNav).slideDown(options.speed);
									$(this).siblings(':not(expanded)').removeClass('expanded').find($childNav).slideUp(options.speed);
	            }
	        });
	    }
	
	    return {
	        init: slider
	    };
	
	}());
	
	accordionSlider.init({
	    speed: 500,
	    openMenu: false,    // set to true to keep menu open ('nth' option will need to be set after)
	    nth: 3,            // choose which item to display as opened (0 based index)
	    headerLinks: true  // true = "a tags are clickable"
	});
	
	// END secondary accordion menu

}());
