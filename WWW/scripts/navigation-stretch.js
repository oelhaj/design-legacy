var UBP = UBP || {};

UBP.equaliseNavigation = UBP.equaliseNavigation || (function () {

	//Equalise navigation item width to span the containing element's width
	
	function doExpand(o) {
		var navigation = $('.lt-ie9 .navigation.primary > ul'),
				navWidth = navigation.innerWidth(),
				defaults = {
            equalWidth: true,
            scalable: true,
            minItems: 4
        },
        options = o;
				
		navigation.css('width','100%').addClass('stretch');
		
		options = $.extend(defaults, options);
		
		var navWidthNew = navigation.innerWidth(),
				navWidthRemain = (navWidthNew - navWidth),
				navItems = navigation.find('li:not(ul li ul li)'),
				navItemsCount = navItems.size();
			
				if (options.scalable) {
					extraWidth = navWidthRemain / navItemsCount;
				} else {
					extraWidth = Math.floor(navWidthRemain / navItemsCount);
				}
		
		if (navItemsCount >= options.minItems) {
			navItems.each(function() {
				var $this = $(this);

				if (options.equalWidth === true) {
					// Equal width navigation items
					var border = parseInt($this.css('border-left-width')) + parseInt($this.css('border-right-width'));
						
					if (options.scalable) {
						var widthNew = (navWidthNew / navItemsCount - border) / navWidthNew * 100 +'%';
					} else {
						var widthNew = Math.floor(navWidthNew / navItemsCount) - border;
					}
					
					$this.css('width',widthNew);
				
				} else if (options.equalWidth === false) {
					// Varying width navigation items based on text length
					var width = parseInt($this.css('width'));
					
					if (options.scalable) {
						var widthNew = (width + extraWidth) / navWidthNew * 100 +'%';
					} else {
						var widthNew = Math.floor(width + extraWidth);
					}

					$this.css('width',widthNew);
				}
			});
		}
	}
	
	return {
		init: doExpand
	};
}())

$(document).ready(function () {
	UBP.equaliseNavigation.init({
		equalWidth: true,		// Equal width or navigation item based width items
		scalable: true,		// Fixed or percentage based scalable navigation items
		minItems: 4 // Apply navigation item stretching only if navigation has more than the minimum items 
	});
});
