var UBP = UBP || {};

UBP.regForms = UBP.regForms || (function () {

	var formSelector;

	function harvestFields($form) {
		var fieldValues = {};
		fieldValues.name = $form.find('[name="name-input"]')[0].value;
		fieldValues.email = $form.find('[name="your-email-input"]')[0].value;
		fieldValues.comments = $form.find('[name="comment-input"]')[0].value;
		fieldValues.fromaddress = $form.find('[name="from-address"]')[0].value;
		fieldValues.toaddress = $form.find('[name="to-address"]')[0].value;
		return fieldValues;
	}

	// event handler
	function submitHandler(form) {
		var postObject,
			$thisForm = $(form),
			request,
			$activityIndicator = $('.m-contact-us .loader-shade');

		$activityIndicator.show();

		postObject = harvestFields($thisForm);

		request = $.ajax({
			url: '/wcf-services/JsonBoilerplateService.svc/addRegistrant',
			type: 'POST',
			data: JSON.stringify({registrant: postObject}),
			processData: true,
			crossDomain: false,
			async: true,
			cache: false,
			contentType: "application/json"
		});

		// result of server validation
		request.done(function (data) {
			switch (data.Status.Value) {
			case 1:
				// data was found to be valid
				$(formSelector).hide();
				$('.thank-you-form').show();
				$activityIndicator.hide();
				break;

			case -1:
				// data was found to be invalid
				$activityIndicator.hide();
				break;

			default:
				$activityIndicator.hide();
				break;
			}
		});

		request.fail(function (jqXHR, textStatus) {
			//console.log("Request failed: " + textStatus);
			$activityIndicator.hide();
		});

	}

	function init(settings) {
		formSelector = settings.formSelector;
		$(formSelector).validate({
			submitHandler: submitHandler,
			rules: {
				'name-input': {
					required: true
				},
				'your-email-input': {
					required: true,
					email: true
				},
				'comment-input': {
					required: true
				}
			},
			messages: {
				'name-input': {
					required: "Please enter your name"
				},
				'your-email-input': {
					required: "Please enter a valid email address"
				},
				'comment-input': {
					required: "Please write a short message."
				}

			}
		});
	}

	return {
		init: init
	};

}());

UBP.regForms.init({
	formSelector: '.registration-form'
});

