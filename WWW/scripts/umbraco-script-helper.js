﻿/* 
    This script is used by datatypes in the back office to manipulate the UI
*/


var UmbracoScriptHelper = UmbracoScriptHelper || (function () {

    // private
    function getTab(tabName) {

        return jQuery('span > nobr')
                .filter(function () {
                    return jQuery(this).html() == tabName;
                })
                .parentsUntil('li', 'a').parent();

    }

    // private
    function getTabArea(tabName) {

        return jQuery('div#' + getTab(tabName).attr('id') + 'layer_contentlayer div.tabpageContent');

    }

    function activateTab(tabName) {

        getTab(tabName).find('a:first').click();

    }

    function hideTab(tabName) {

        getTab(tabName).hide();

    }

    function findPropertyInTab(tabName, propertyName) {

        return getTabArea(tabName)
                .find('div.propertyItemheader')
                .filter(function () {
                    return jQuery(this).html() == propertyName;
                })
                .parentsUntil('div.tabpagecontent', 'div.propertypane');

    }

    function movePropertyToTop(tabName, propertyName) {

        findPropertyInTab(tabName, propertyName)
            .prependTo(getTabArea(tabName));

    }

    function movePropertyToBottom(tabName, propertyName) {

        // TODO:

    }

    return {

        activateTab: activateTab,
        hideTab: hideTab,
        findPropertyInTab: findPropertyInTab,
        movePropertyToTop: movePropertyToTop

    };

} ());